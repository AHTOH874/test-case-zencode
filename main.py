########################################
##                                    ##
##    Coded by Anton with love <3.    ##
##                                    ##
########################################


"""
Simple bot for send pictures and videos.
"""
import logging
import requests

from telegram.ext import Updater, CommandHandler

logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
                    level=logging.INFO)
logger = logging.getLogger(__name__)

def more(update, context):
    try:
        raw_response = requests.get('https://random.dog/woof.json')
    except (requests.exceptions.ConnectionError, requests.exceptions.Timeout) as e:
        update.message.reply_text('Произошла ошибка подключения к сервису с собаками')
        logger.warning('Request to random.dog is closed: %s', e)

    json_response = raw_response.json()
    url = json_response['url']
    if any(x in url for x in ['jpg', 'JPG', 'png', 'PNG']):
        update.message.reply_photo(photo=url)
    elif any(x in url for x in ['mp4', 'webm']):
        update.message.reply_video(video=url)
    elif any(x in url for x in ['gif']):
        update.message.reply_animation(animation=url)
    else:
        update.message.reply_text(url)


def start(update, context):
   update.message.reply_text('Hello! Type /more to resolve pictures.')


def help(update, context):
    update.message.reply_text("Use /more to see dogs :)")


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("YOUR_TOKEN", use_context=True)

    updater.dispatcher.add_handler(CommandHandler('more', more))
    updater.dispatcher.add_handler(CommandHandler('start', start))
    updater.dispatcher.add_handler(CommandHandler('help', help))
    updater.dispatcher.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT
    updater.idle()


if __name__ == '__main__':
    main()