# Simple telegram bot
How to use it? 
1. Run cmd `pip install -r requirements.txt`
2. Replace `YOUR_TOKEN` in `main.py`
``` 
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater("YOUR_TOKEN", use_context=True)
```
4. Run ```python main.py```
3. Be happy!
